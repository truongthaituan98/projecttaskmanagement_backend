package com.example.demo.services

import com.example.demo.common.MapperUtil
import com.example.demo.domain.dto.ProjectCreateDTO
import com.example.demo.domain.dto.ProjectDTO
import com.example.demo.domain.dto.ProjectUpdateDTO
import com.example.demo.domain.entities.ApplicationUser
import com.example.demo.domain.entities.Backlog
import com.example.demo.domain.entities.Project
import com.example.demo.exception.CustomException
import com.example.demo.repositories.ProjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.http.HttpStatus


@Service
class ProjectServiceImpl : ProjectService {
    @Autowired
    lateinit var projectRepository: ProjectRepository
    @Autowired
    lateinit var backlogService: BacklogService

    override fun findAllProjectByEmail(email: String): List<ProjectDTO> {
        var projects = projectRepository.findAllProjectsByEmail(email)
        val projectDTO = ArrayList<ProjectDTO>()
        for (project in projects) {
            projectDTO.add(MapperUtil.projectToProjectDTO(project))
        }
        return projectDTO
    }

    override fun getProjectByProjectIdentifier(projectIdentifier: String): Any {
        val project = projectRepository.findByprojectIdentifier(projectIdentifier)
        return if(!project.isPresent){
            throw CustomException("Project $projectIdentifier not found")
        }else {
            MapperUtil.projectToProjectDTO(project.get())
        }
    }

    override fun updateProject(user: ApplicationUser, projectIdentifier: String, newProject: ProjectUpdateDTO):Any {
        var project = this.projectRepository.findByprojectIdentifier(projectIdentifier)
        return if(!project.isPresent){
            throw CustomException("Project $projectIdentifier not found")
        }else{
           var projectUpdated = Project.updateProjectDTOToProject(newProject)
            projectUpdated.user = project.get().user
            ResponseEntity<Any>(MapperUtil.projectToProjectDTO(projectRepository.save(projectUpdated)), HttpStatus.OK)
        }
  }

    override fun insertProject(user: ApplicationUser,projectCreateDTO: ProjectCreateDTO): Any {
        val project = this.projectRepository.findByprojectIdentifier(projectCreateDTO.projectIdentifier)
            if(!project.isPresent){
                if((projectCreateDTO.endDate.compareTo(projectCreateDTO.startDate)) >= 0){
                    val project: Project = Project.createProjectDTOToProject(projectCreateDTO)
                    project.user = user
                    val projectNew = projectRepository.save(project)
                    val backlog = Backlog()
                    backlog.project = projectNew
                    backlogService.insertBacklog(backlog)
                    return MapperUtil.projectToProjectDTO(projectNew)
                }
                else{
                    throw CustomException("${projectCreateDTO.endDate} should not less than startDate: ${projectCreateDTO.startDate}")
                }
            }else{
                throw CustomException("Project ${projectCreateDTO.projectIdentifier} is already exist!")
            }


    }

    override fun deleteProject(projectIdentifier: String): ResponseEntity<Any> {
        val project = projectRepository.findByprojectIdentifier(projectIdentifier)
        return if (!project.isPresent) {
            throw CustomException("Project $projectIdentifier not found")
        } else {
            projectRepository.delete(project.get())
            ResponseEntity<Any>("Delete Successfully", HttpStatus.OK)
        }
    }
}
