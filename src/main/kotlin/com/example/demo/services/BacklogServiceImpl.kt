package com.example.demo.services

import com.example.demo.common.MapperUtil
import com.example.demo.domain.dto.BacklogDTO
import com.example.demo.domain.entities.Backlog
import com.example.demo.repositories.BacklogRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import kotlin.collections.ArrayList


@Service
class BacklogServiceImpl : BacklogService {
    @Autowired
    lateinit var backlogRepository: BacklogRepository


    override fun updateBacklog(backlog: Backlog): BacklogDTO = MapperUtil.backlogToDTO(backlogRepository.save(backlog))



    override fun getBacklogById(backlogId: Long): BacklogDTO {
        return MapperUtil.backlogToDTO(backlogRepository.findById(backlogId).get())
    }

    override fun insertBacklog(backlog: Backlog): BacklogDTO = MapperUtil.backlogToDTO(backlogRepository.save(backlog))

//    override fun deleteBacklog(backlogId: Long): ResponseEntity<String>? {
//        return backlogRepository.findById(backlogId).map { backlog  ->
//            backlogRepository.delete(backlog)
//            ResponseEntity<String>("Delete Successfully",HttpStatus.OK)
//        }.orElseThrow { BacklogNotFoundException(backlogId) }
//    }

    override fun findAllBacklog(): ArrayList<BacklogDTO> {

        var backlogs= backlogRepository.findAll() as ArrayList<Backlog>
        val backlogDTO = ArrayList<BacklogDTO>()
        for (backlog in backlogs) {
            print(backlog)
            backlogDTO.add(MapperUtil.backlogToDTO(backlog))
        }
        for (b in backlogDTO){
            println(b.projectIdentifier)
        }
        return backlogDTO
    }
}
