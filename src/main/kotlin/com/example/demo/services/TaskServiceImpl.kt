package com.example.demo.services

import com.example.demo.common.MapperUtil
import com.example.demo.domain.dto.TaskCreateDTO
import com.example.demo.domain.dto.TaskDTO
import com.example.demo.domain.dto.TaskUpdateDTO
import com.example.demo.domain.entities.Task
import com.example.demo.domain.entities.TaskTotal
import com.example.demo.exception.CustomException
import com.example.demo.repositories.BacklogRepository
import com.example.demo.repositories.ProjectRepository
import com.example.demo.repositories.TaskRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList


@Service
class TaskServiceImpl: TaskService {

    @Autowired
    lateinit var taskRepository: TaskRepository
    @Autowired
    lateinit var backlogRepository: BacklogRepository
    @Autowired
    lateinit var taskTotalService: TaskTotalService
    @Autowired
    lateinit var projectRepository: ProjectRepository

    override fun getTaskByProjectTaskSequence(projectIdentifier: String, projectTaskSequence: String): Any {
        val project = projectRepository.findByprojectIdentifier(projectIdentifier)
        val task = taskRepository.getTaskByprojectTaskSequence(projectTaskSequence)
         if(!project.isPresent){
           throw CustomException("Project $projectIdentifier not found")
        }
        if(!task.isPresent){
                throw CustomException("Project Task: $projectTaskSequence not found")
            }
            return taskRepository.showTaskByProjectTaskSequence(projectIdentifier, projectTaskSequence).get()
    }
    override fun findAllTaskByProjectIdentifierTest(projectIdentifier: String): ArrayList<Task> {
            return taskRepository.showAllTask(projectIdentifier)
    }
    override fun updateByProjectTaskSequence(newTask: TaskUpdateDTO,
                                             projectIdentifier: String, projectTaskSequence: String): Any {
        val task = this.taskRepository.showTaskByProjectTaskSequence(projectIdentifier, projectTaskSequence)
        return if(!task.isPresent){
            throw CustomException("Task with projectTaskSequence = $projectTaskSequence not found")
        }else{
            val backlog = backlogRepository.getBacklogIdByProjectIdentifier(projectIdentifier)
            if(!backlog.isPresent){
                throw CustomException("Backlog ${newTask.backlog!!.backlogId} not found")
            }else{
                newTask.projectTaskSequence = projectTaskSequence
                newTask.backlog = backlog.get()
                task.get().updateTaskDTOToTask(newTask)
                taskRepository.save(task.get())
                MapperUtil.taskToDTO(taskRepository.save(task.get()))
            }
        }
    }

    override fun deleteByProjectTaskSequence(projectIdentifier: String, projectTaskSequence: String) {
        val project = projectRepository.findByprojectIdentifier(projectIdentifier)
        val task = taskRepository.getTaskByprojectTaskSequence(projectTaskSequence)
         if (!project.isPresent) {
            throw CustomException("Project $projectIdentifier not found")
        } else
            if (!task.isPresent) {
                throw CustomException("Project Task: $projectTaskSequence not found")
            } else {
                val taskByProjectIdentifierAndTaskSequence =
                        taskRepository.showTaskByProjectTaskSequence(projectIdentifier, projectTaskSequence).get()
                taskRepository.delete(taskByProjectIdentifierAndTaskSequence)
            }
    }
    override fun deleteByProjectTaskSequenceTest(projectIdentifier: String, projectTaskSequence: String) {
        val taskByProjectIdentifierAndTaskSequence =
                taskRepository.showTaskByProjectTaskSequenceTest(projectIdentifier, projectTaskSequence)
        taskRepository.delete(taskByProjectIdentifierAndTaskSequence)
    }

    override fun findAllTaskByProjectIdentifier(projectIdentifier: String): List<TaskDTO> {
        val project = projectRepository.findByprojectIdentifier(projectIdentifier)
        if(!project.isPresent){
            throw CustomException("Project $projectIdentifier not found")
        }else{
            var tasks = taskRepository.showAllTask(projectIdentifier)
            val taskDTO = ArrayList<TaskDTO>()
            for (task in tasks) {
                taskDTO.add(MapperUtil.taskToDTO(task))
            }
            return taskDTO
        }
    }
    override fun createBacklogByProjectIdentifier(taskCreateDTO: TaskCreateDTO, projectIdentifier: String): Any {
        if(!projectRepository.findByprojectIdentifier(projectIdentifier).isPresent){
            throw CustomException("Project $projectIdentifier not found")
        }else{
            val task: Task = Task.createTaskDTOToTask(taskCreateDTO)
            val backlogByProjectIdentifier = backlogRepository.getBacklogIdByProjectIdentifier(projectIdentifier).get()
            task.backlog = backlogByProjectIdentifier
            val taskTotal: TaskTotal = taskTotalService.insertBacklogIntoTaskTotal(backlogByProjectIdentifier.backlogId) as TaskTotal
            var stringJoiner = StringJoiner("-")
            stringJoiner.add(projectIdentifier)
            stringJoiner.add(taskTotal.total.toString())
            task.projectTaskSequence = stringJoiner.toString()
            return  MapperUtil.taskToDTO(taskRepository.save(task))
        }
    }
}
