package com.example.demo.services

import com.example.demo.domain.entities.TaskTotal
import com.example.demo.exception.CustomException
import com.example.demo.repositories.BacklogRepository
import com.example.demo.repositories.TaskTotalRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import kotlin.collections.ArrayList


@Service
class TaskTotalServiceImpl : TaskTotalService {
    override fun findAllTaskTotal(): List<TaskTotal> = taskTotalRepository.findAll()
    @Autowired
    lateinit var taskTotalRepository: TaskTotalRepository
    @Autowired
    lateinit var backlogRepository: BacklogRepository
    override fun insertBacklogIntoTaskTotal(backlogId: Long?):Any {
        return if (!backlogRepository.findById(backlogId!!).isPresent){
           throw CustomException("Backlog $backlogId not found")
        }else {
            val backlogTask = backlogRepository.checkProjectExist(backlogId)
            var taskTotal = TaskTotal()
            if (!backlogTask.isPresent) {
                taskTotal.total = 1
                taskTotal.backlogId = backlogId

            } else {
                val listTaskTotal: ArrayList<TaskTotal> = taskTotalRepository.getAllTaskTotal(backlogId)
                for (i in 1..listTaskTotal.size){
                    taskTotal.total = listTaskTotal[listTaskTotal.size - 1].total!!.plus(1)
                    taskTotal.backlogId = backlogId
                }
            }
            return taskTotalRepository.save(taskTotal)
        }
    }
}