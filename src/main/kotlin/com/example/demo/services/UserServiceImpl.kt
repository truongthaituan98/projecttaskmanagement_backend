package com.example.demo.services

import com.example.demo.common.MapperUtil
import com.example.demo.domain.dto.CustomUserDetail
import com.example.demo.security.JwtUtil
import com.example.demo.domain.dto.Response
import com.example.demo.domain.dto.UserDTO
import com.example.demo.domain.dto.UserRegisterDto
import com.example.demo.domain.entities.ApplicationUser
import com.example.demo.exception.CustomException
import com.example.demo.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import javax.naming.AuthenticationException


@Service
class UserServiceImpl(val userRepository: UserRepository, val bCryptPasswordEncoder: BCryptPasswordEncoder) : UserService {
    @Autowired
    private val authenticationManager: AuthenticationManager? = null
    override fun findAll(): List<UserDTO> {
        val users = userRepository.findAll()
        return MapperUtil.mapList(users,UserDTO::class.java)
    }

    override fun registerUser(userRegisterDto: UserRegisterDto): Any {
//        if (userRepository.isEmailExist(userRegisterDto.email) > 0) {
//            throw EmailAlreadyExistsException("Email ${userRegisterDto.email} is already exists")
//        }
        if (!userRegisterDto.password.equals(userRegisterDto.confirmPassword)) {
            throw CustomException("Password don't match")
        }
            val user = ApplicationUser(userRegisterDto.fullName,
                    userRegisterDto.email,
                    (bCryptPasswordEncoder.encode(userRegisterDto.password)))
            return MapperUtil.userToDTO(userRepository.save(user))

    }

    @Autowired
    private val jwtUtil: JwtUtil? = null
    override fun login(username: String, password: String): Response {
        var response: Response? = null
        try {
            val authentication = authenticationManager?.authenticate(
                    UsernamePasswordAuthenticationToken(
                            username,
                            password))
            SecurityContextHolder.getContext().authentication = authentication
            println("alo"+ SecurityContextHolder.getContext().authentication.name)
            val role = SecurityContextHolder.getContext().authentication.authorities.toString()
            response = Response(jwtUtil?.generateTokenForUser(authentication?.principal as CustomUserDetail),
                    true, username, role)
        } catch (ex: AuthenticationException) {
           throw CustomException("Username or Password is incorrect")
        }

        return response
    }

}
