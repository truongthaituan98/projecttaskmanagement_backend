package com.example.demo.services

import com.example.demo.domain.dto.TaskCreateDTO
import com.example.demo.domain.dto.TaskDTO
import com.example.demo.domain.dto.TaskUpdateDTO
import com.example.demo.domain.entities.Task
import org.springframework.http.ResponseEntity


interface TaskService{
    fun findAllTaskByProjectIdentifier(projectIdentifier: String): List<TaskDTO>
    fun createBacklogByProjectIdentifier(taskCreateDTO: TaskCreateDTO, projectIdentifier: String): Any
    fun updateByProjectTaskSequence(task: TaskUpdateDTO, projectIdentifier: String, projectTaskSequence: String): Any
    fun getTaskByProjectTaskSequence(projectIdentifier: String, projectTaskSequence: String): Any
    fun deleteByProjectTaskSequence(projectIdentifier: String, projectTaskSequence: String)
    fun findAllTaskByProjectIdentifierTest(projectIdentifier: String): ArrayList<Task>
    fun deleteByProjectTaskSequenceTest(projectIdentifier: String, projectTaskSequence: String)
}