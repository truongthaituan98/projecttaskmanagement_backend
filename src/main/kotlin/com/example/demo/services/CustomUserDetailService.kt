package com.example.demo.services

import com.example.demo.domain.dto.CustomUserDetail
import com.example.demo.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service


@Service
class CustomUserDetailService @Autowired
constructor(private val userRepository: UserRepository) : UserDetailsService {
    override fun loadUserByUsername(email: String): UserDetails {
        val u = userRepository.findByemail(email)
        if(!u.isPresent){
            throw UsernameNotFoundException("Email Not Found")
        }else{
            return CustomUserDetail(u.get())
        }
    }
}
