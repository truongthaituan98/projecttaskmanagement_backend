package com.example.demo.services

import com.example.demo.domain.entities.TaskTotal

interface TaskTotalService{
    fun insertBacklogIntoTaskTotal(backlogId: Long?):Any
    fun findAllTaskTotal(): List<TaskTotal>
}