package com.example.demo.services

import com.example.demo.domain.dto.ProjectCreateDTO
import com.example.demo.domain.dto.ProjectDTO
import com.example.demo.domain.dto.ProjectUpdateDTO
import com.example.demo.domain.entities.ApplicationUser
import com.example.demo.domain.entities.Project
import org.springframework.http.ResponseEntity


interface ProjectService {
    fun findAllProjectByEmail(email: String): List<ProjectDTO>
    fun getProjectByProjectIdentifier(projectIdentifier: String): Any
    fun insertProject(user: ApplicationUser,projectCreateDTO: ProjectCreateDTO): Any
    fun updateProject(user: ApplicationUser, projectIdentifier: String, newProject: ProjectUpdateDTO): Any
    fun deleteProject(projectIdentifier: String): ResponseEntity<Any>
}
