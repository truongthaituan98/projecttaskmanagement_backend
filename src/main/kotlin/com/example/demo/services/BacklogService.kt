package com.example.demo.services

import com.example.demo.domain.dto.BacklogDTO
import com.example.demo.domain.entities.Backlog


interface BacklogService{
    fun findAllBacklog(): List<BacklogDTO>
    fun getBacklogById(backlogId: Long): BacklogDTO
    fun insertBacklog(backlog: Backlog): BacklogDTO
    fun updateBacklog(backlog: Backlog): BacklogDTO
//    fun deleteBacklog(backlogId: Long): ResponseEntity<String>?
}
