package com.example.demo.services


import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError

import java.util.HashMap

@Service
class MapValidationErrorService {

    fun mapValidationService(result: BindingResult): ResponseEntity<*>? {

        if (result.hasErrors()) {
            val errorMap = HashMap<String, String?>()

            for (error in result.fieldErrors) {
                errorMap[error.field] = error.defaultMessage
            }
            return ResponseEntity<Any>(errorMap, HttpStatus.BAD_REQUEST)
        }
        return null
    }
}