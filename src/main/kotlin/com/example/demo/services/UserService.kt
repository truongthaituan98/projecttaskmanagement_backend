package com.example.demo.services

import com.example.demo.domain.dto.Response
import com.example.demo.domain.dto.UserDTO
import com.example.demo.domain.dto.UserRegisterDto


interface UserService {
    fun findAll(): List<UserDTO>
//    fun getUserById(id: Long?): UserDTO
    fun registerUser(userRegisterDto: UserRegisterDto): Any
    fun login(userName: String, password: String): Response
//    fun updateUser(user: ApplicationUser): UserDTO
//    fun deleteUser(id: Long?)
//    fun findByuserName(username: String): UserDTO
//    //    Boolean changePassword(UserUpdateDTO userUpdateDTO);
//    fun login(username: String, password: String): Response
}
