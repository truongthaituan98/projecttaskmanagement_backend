package com.example.demo.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "users")
class ApplicationUser(
        @Column(name = "full_name")
        var fullName: String? = null,
        @Column(name = "email")
        var email: String? = null,
        @Column(name = "password")
        var password: String? = null
) {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var userId: Long? = null

    @JsonIgnore
    @OneToMany(
            cascade = [CascadeType.DETACH, CascadeType.REMOVE, CascadeType.REFRESH],
            mappedBy = "user",
            fetch = FetchType.EAGER
    )
    val projects: MutableSet<Project>? = mutableSetOf()

}
