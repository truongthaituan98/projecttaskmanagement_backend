package com.example.demo.domain.entities

import javax.persistence.*

@Entity
@Table(name = "tasktotals")
class TaskTotal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var taskId: Long? = null
    @Column(name = "backlog_id")
    var backlogId: Long? = null
    @Column(name = "total")
    var total: Int? = null
}
