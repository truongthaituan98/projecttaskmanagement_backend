package com.example.demo.domain.entities

import com.example.demo.domain.dto.ProjectCreateDTO
import com.example.demo.domain.dto.ProjectUpdateDTO
import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "projects")
data class Project(
        @Id
        @Column(name = "project_identifier")
        var projectIdentifier: String,
        @Column(name = "project_name")
        var projectName: String,
        @Column(name = "description")
        var description: String,
        @Column(name = "start_date")
        var startDate: LocalDate,
        @Column(name = "end_date")
        var endDate: LocalDate,
        @Column(name = "created_at")
        var createdAt: LocalDate,
        @Column(name = "updated_at")
        var updatedAt: LocalDate
) {

        @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.REFRESH])
        @JoinColumn(name = "user_id")
        var user: ApplicationUser? = null

        @JsonIgnore
        @OneToOne(
        fetch = FetchType.LAZY,
        mappedBy = "project",
        cascade = [CascadeType.ALL]
        )
        var backlog: Backlog? = null
        fun updateProject(projectIdentifier: String, projectName: String, description: String,
                startDate: LocalDate, endDate: LocalDate, createdAt: LocalDate, updatedAt: LocalDate)
        {
                this.projectIdentifier = projectIdentifier
                this.projectName = projectName
                this.description = description
                this.startDate = startDate
                this.endDate = endDate
                this.createdAt = createdAt
                this.updatedAt = updatedAt

    }
    companion object {
        fun createProjectDTOToProject(projectCreateDTO: ProjectCreateDTO) = Project(
                projectIdentifier = projectCreateDTO.projectIdentifier,
                projectName = projectCreateDTO.projectName,
                description = projectCreateDTO.description,
                startDate =  LocalDate.parse(projectCreateDTO.startDate),
                endDate =  LocalDate.parse(projectCreateDTO.endDate),
                createdAt = LocalDate.now(),
                updatedAt = LocalDate.now()
        )
        fun updateProjectDTOToProject(projectUpdateDTO: ProjectUpdateDTO) = Project(
                projectIdentifier = projectUpdateDTO.projectIdentifier,
                projectName = projectUpdateDTO.projectName,
                description = projectUpdateDTO.description,
                startDate =  LocalDate.parse(projectUpdateDTO.startDate),
                endDate =  LocalDate.parse(projectUpdateDTO.endDate),
                createdAt = LocalDate.parse(projectUpdateDTO.createdAt),
                updatedAt = LocalDate.now()
        )
    }

}
