package com.example.demo.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "backlogs")
 class Backlog(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "backlog_id")
        var backlogId: Long? = null,
        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "project_identifier", referencedColumnName = "project_identifier")
        var project: Project? = null
)
{

    @JsonIgnore
    @OneToMany(cascade = [(CascadeType.ALL)], fetch = FetchType.LAZY, mappedBy = "backlog")
      val tasks = mutableListOf<Task>()

}
