package com.example.demo.domain.entities

import com.example.demo.domain.dto.TaskCreateDTO
import com.example.demo.domain.dto.TaskUpdateDTO
import java.time.LocalDate
import javax.persistence.*


@Entity
@Table(name = "tasks")
class Task(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "task_id")
        var taskId: Long? = null,
        @Column(name = "summary")
        var summary: String? = null,
        @Column(name = " acceptance_criteria")
        var acceptanceCriteria: String? = null,
        @Column(name = "status")
        var status: String? = null,
        @Column(name = "priority")
        var priority: String? = null,
        @Column(name = "due_date")
        var dueDate: LocalDate? = null,
        @Column(name = "created_at")
        var createdAt: LocalDate? = null,
        @Column(name = "updated_at")
        var updatedAt: LocalDate? = null,
        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "backlog_id")
        var backlog: Backlog? = null,
        @Column(name = "project_task_sequence")
        var projectTaskSequence: String? = null
) {



    @OneToMany(
            cascade = [CascadeType.DETACH, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH],
            mappedBy = "backlog",
            fetch = FetchType.EAGER
    )
    var tasks: MutableList<Task> = mutableListOf()


    fun updateTask(backlog: Backlog, projectTaskSequence: String, summary: String,acceptanceCriteria: String,
                status: String, priority: String, dueDate: LocalDate, createdAt: LocalDate, updatedAt: LocalDate) {
        this.backlog = backlog
        this.projectTaskSequence = projectTaskSequence
        this.summary = summary
        this.acceptanceCriteria = acceptanceCriteria
        this.status = status
        this.priority = priority
        this.dueDate = dueDate
        this.createdAt = createdAt
        this.updatedAt = updatedAt
    }

    companion object {
        fun createTaskDTOToTask(taskCreateDTO: TaskCreateDTO): Task {
            if (taskCreateDTO.dueDate != "") {
                return Task(
                        summary = taskCreateDTO.summary,
                        acceptanceCriteria = taskCreateDTO.acceptanceCriteria,
                        status = taskCreateDTO.status,
                        priority = taskCreateDTO.priority,
                        dueDate = LocalDate.parse(taskCreateDTO.dueDate),
                        createdAt = LocalDate.now(),
                        updatedAt = LocalDate.now()
                )
            } else {
                return Task(
                        summary = taskCreateDTO.summary,
                        acceptanceCriteria = taskCreateDTO.acceptanceCriteria,
                        status = taskCreateDTO.status,
                        priority = taskCreateDTO.priority,
                        dueDate = LocalDate.now(),
                        createdAt = LocalDate.now(),
                        updatedAt = LocalDate.now()
                )
            }
        }
    }
        fun updateTaskDTOToTask(taskUpdateDTO: TaskUpdateDTO){
                    this.backlog = taskUpdateDTO.backlog
                    this.projectTaskSequence = taskUpdateDTO.projectTaskSequence
                    this.summary = taskUpdateDTO.summary
                    this.acceptanceCriteria = taskUpdateDTO.acceptanceCriteria
                    this.status = taskUpdateDTO.status
                    this.priority =  taskUpdateDTO.priority
                    this.dueDate =  LocalDate.parse(taskUpdateDTO.dueDate)
                    this.createdAt = LocalDate.now()
                    this.updatedAt = LocalDate.now()

    }
}
