package com.example.demo.domain.dto

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size
import javax.validation.constraints.Pattern



data class UserDTO(  var userId: Long? = null,
                var fullName: String? = null,
                var email: String? = null)
data class UserRegisterDto(
        @get:NotBlank(message = "fullName is required")
        @get:Size(min = 4, max = 20)
        val fullName: String,
        @Email
        @get:NotBlank(message = "Email is required")
        val email: String,
        @get:NotBlank(message = "Password is required")
        @get:Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%^&+=])(?=\\S+\$).{8,40}\$",
                message = "Your password is not strong enough")
        var password: String,
        @get:NotBlank(message = "Confirm Password is required")
        val confirmPassword: String

)
