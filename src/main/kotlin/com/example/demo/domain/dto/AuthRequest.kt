package com.example.demo.domain.dto

import javax.validation.constraints.NotBlank


data class AuthRequest(
        @get:NotBlank(message = "Email is required")
        var email: String,
        @get:NotBlank(message = "password is required")
        var password: String
)
