package com.example.demo.domain.dto

import com.example.demo.domain.entities.ApplicationUser
import java.time.LocalDate
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size


data class ProjectDTO(
        val projectIdentifier: String,
        val projectName: String,
        val description: String,
        val startDate: LocalDate,
        val endDate: LocalDate,
        val createdAt: LocalDate?,
        val updatedAt: LocalDate?,
        val email: String
)
data class ProjectCreateDTO(
        @get:NotBlank(message = "ProjectIdentifier is required")
        @get:Size(min = 5,max = 5, message = "Project identifier must have 5 characters")
        val projectIdentifier: String,
        @get:NotBlank(message = "Project name is required")
        val projectName: String,
        @get:NotBlank(message = "Project description is required")
        val description: String,
        @get:NotEmpty(message = "StartDate is required")
        val startDate: String,
        @get:NotEmpty(message = "EndDate is required")
        val endDate: String
)
data class ProjectUpdateDTO(
        val projectIdentifier: String,
        @get:NotBlank(message = "Project name is required")
        val projectName: String,
        @get:NotBlank(message = "Project description is required")
        val description: String,
        @get:NotEmpty(message = "StartDate is required")
        val startDate: String,
        @get:NotEmpty(message = "EndDate is required")
        val endDate: String,
        @get:NotEmpty(message = "CreatedAt is required")
        val createdAt: String
)




