package com.example.demo.domain.dto
import com.example.demo.domain.entities.Backlog
import java.time.LocalDate
import javax.validation.constraints.NotBlank

data class TaskDTO(var taskId: Long,
              var backlogId: Long,
              var projectTaskSequence: String,
              var summary: String,
              var acceptanceCriteria: String,
              var status: String,
              var priority: String,
              var dueDate: LocalDate,
              var createdAt: LocalDate?,
              var updatedAt: LocalDate?
)

data class TaskCreateDTO(
        @get:NotBlank(message = "summary is required")
        val summary: String,
        @get:NotBlank(message = "acceptanceCriteria is required")
        val acceptanceCriteria: String,
        @get:NotBlank(message = "status is required")
        val status: String,
        @get:NotBlank(message = "priority is required")
        var priority: String,
        var dueDate: String?
)

data class TaskUpdateDTO(
        var backlog: Backlog?,
        var projectTaskSequence: String?,
        @get:NotBlank(message = "summary is required")
        val summary: String,
        @get:NotBlank(message = "acceptanceCriteria is required")
        val acceptanceCriteria: String,
        @get:NotBlank(message = "status is required")
        val status: String,
        @get:NotBlank(message = "priority is required")
        var priority: String,
        var dueDate: String,
        var createdAt: LocalDate,
        var updatedAt: LocalDate
)

