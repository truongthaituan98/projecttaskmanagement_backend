package com.example.demo.domain.dto


class Response {
    var token: String? = null
    var loggedIn: Boolean? = null
    var userName: String? = null
    var role: String? = null

    constructor(token: String?, isLoggedIn: Boolean?, userName: String, role: String) {
        this.token = token
        this.loggedIn = isLoggedIn
        this.userName = userName
        this.role = role
    }
}
