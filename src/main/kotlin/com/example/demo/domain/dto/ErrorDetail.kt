package com.example.demo.domain.dto

import com.fasterxml.jackson.annotation.JsonInclude

data class ErrorDetail(
        var message: String,
        var status: String,
        @JsonInclude(JsonInclude.Include.NON_NULL)
        val errors: MutableMap<String, String?>? = null


)