package com.example.demo.domain.dto

import com.example.demo.domain.entities.ApplicationUser
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.ArrayList


class CustomUserDetail(val applicationUser: ApplicationUser) : UserDetails {

    override fun getAuthorities(): MutableCollection<GrantedAuthority> {
        val authorities = ArrayList<GrantedAuthority>()
        for (project in applicationUser.projects!!){
            authorities.add(SimpleGrantedAuthority(project.projectIdentifier))
        }

        return authorities    }

    override fun isEnabled(): Boolean = true

    override fun getPassword(): String? = applicationUser.password

    override fun getUsername(): String? = applicationUser.email

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun isCredentialsNonExpired(): Boolean = true

}
