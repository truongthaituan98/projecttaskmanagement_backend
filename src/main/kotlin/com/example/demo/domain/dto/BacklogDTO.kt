package com.example.demo.domain.dto

class BacklogDTO {
    var backlogId: Long? = null
    var projectIdentifier: String? = null
}
