package com.example.demo.security

import com.example.demo.services.CustomUserDetailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtFilter : OncePerRequestFilter() {

    @Autowired
    private val customUserDetailService: CustomUserDetailService? = null

    @Autowired
    private val jwtUtil: JwtUtil? = null

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse,
                                  filterChain: FilterChain) {
        val jwt = getJwtFromRequest(request)

        if (StringUtils.hasText(jwt) && jwtUtil!!.validateToken(jwt)) {
            val userName = jwtUtil.getUsernameFromJWT(jwt)
            try {
                val userDetails: UserDetails = customUserDetailService!!.loadUserByUsername(userName)
                if (userDetails != null) {
                    val authentication = UsernamePasswordAuthenticationToken(userDetails, null,
                            userDetails.authorities)
                    authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
                    SecurityContextHolder.getContext().authentication = authentication

                }
            } catch (ex: Exception) {
               print(ex.stackTrace)
            }

        }

        filterChain.doFilter(request, response)
    }

    private fun getJwtFromRequest(request: HttpServletRequest): String? {

        val bearerToken = request.getHeader("Authorization")
        return if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            bearerToken.substring(7)
        } else null
    }
}
