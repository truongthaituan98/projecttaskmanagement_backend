package com.example.demo.security

import com.example.demo.services.CustomUserDetailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.stereotype.Component
import org.springframework.security.core.userdetails.UserDetails


@Component
class UserAuthenticationProvider : AuthenticationProvider {

    @Autowired
    internal var customUserDetailService: CustomUserDetailService? = null

    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication? {
        try {
            val user: UserDetails = customUserDetailService!!.loadUserByUsername(authentication.name)

            var result: UsernamePasswordAuthenticationToken? = null
            // Check user
            if (user != null) {
                if (user.username == authentication.name && user.password == authentication.credentials.toString()) {
                    result = UsernamePasswordAuthenticationToken(user.username, user.password, user.authorities)

                }
            } else {
                throw BadCredentialsException("User authentication failed")
            }
            return result
        } catch (ex: Exception) {
            throw BadCredentialsException("User authentication failed")
        }

    }

    override fun supports(authentication: Class<*>): Boolean {
        return authentication == UsernamePasswordAuthenticationToken::class.java
    }
}
