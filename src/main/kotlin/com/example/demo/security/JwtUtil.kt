package com.example.demo.security

import com.example.demo.domain.dto.CustomUserDetail
import io.jsonwebtoken.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.GrantedAuthority
import org.springframework.stereotype.Service

import java.util.Date
import java.util.stream.Collectors

@Service
class JwtUtil {
    @Value("3600000")
    private val JWT_EXPIRATION: Long = 3600000

    // Generate JWT Token base on user's information
    fun generateTokenForUser(userDetails: CustomUserDetail): String {
        val authorities = userDetails.authorities.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","))
        val now = Date()
        val expiryDate = Date(now.time + JWT_EXPIRATION)

        return Jwts.builder()
                .setSubject(userDetails.username)
                .claim("rol", authorities)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.JWT_SECRET)
                .compact()
    }

    fun getUsernameFromJWT(token: String?): String {
        val claims = Jwts.parser()
                .setSigningKey(SecurityConstants.JWT_SECRET)
                .parseClaimsJws(token)
                .body
        return claims.subject
    }

    fun validateToken(authToken: String?): Boolean {
        try {
            Jwts.parser().setSigningKey(SecurityConstants.JWT_SECRET).parseClaimsJws(authToken)
            return true
        } catch (ex: MalformedJwtException) {
            println("Invalid JWT token")
        } catch (ex: ExpiredJwtException) {
            println("Expired JWT token")
        } catch (ex: UnsupportedJwtException) {
            println("Unsupported JWT token")
        } catch (ex: IllegalArgumentException) {
            println("JWT claims string is empty.")
        }

        return false
    }
}

