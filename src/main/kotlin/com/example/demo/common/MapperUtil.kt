package com.example.demo.common

import com.example.demo.domain.dto.BacklogDTO
import com.example.demo.domain.dto.ProjectDTO
import com.example.demo.domain.dto.TaskDTO
import com.example.demo.domain.dto.UserDTO
import com.example.demo.domain.entities.ApplicationUser
import com.example.demo.domain.entities.Backlog
import com.example.demo.domain.entities.Project
import com.example.demo.domain.entities.Task
import org.modelmapper.ModelMapper
import org.modelmapper.convention.MatchingStrategies
import org.springframework.data.domain.Page
import java.util.ArrayList

object MapperUtil {
    private val modelMapper: ModelMapper

    init {
        modelMapper = ModelMapper()
        modelMapper.configuration.matchingStrategy = MatchingStrategies.STRICT
    }

    fun <D, T> mapPage(entities: Page<T>, dtoClass: Class<D>): Page<D> {
        return entities.map { objectEntity -> modelMapper.map(objectEntity, dtoClass) }

    }

    fun <D, T> mapObject(entity: T, dtoClass: Class<D>): D {
        return modelMapper.map(entity, dtoClass)
    }


    fun <S, T> mapList(source: List<S>, targetClass: Class<T>): List<T> {
        val list = ArrayList<T>()
        for (s in source) {
            list.add(modelMapper.map(s, targetClass))
        }
        return list
    }

    fun <S, D> map(source: S, destination: D): D {
        modelMapper.map(source, destination)
        return destination
    }
    fun backlogToDTO(backlog: Backlog) : BacklogDTO{
        val backlogDTO = BacklogDTO()
        backlogDTO.backlogId = backlog.backlogId
        backlogDTO.projectIdentifier = backlog.project!!.projectIdentifier
        return backlogDTO
    }
    fun projectToProjectDTO(project: Project) : ProjectDTO{
        return ProjectDTO(project.projectIdentifier!!,project.projectName!!,
                project.description!!,project.startDate!!, project.endDate!!,
                project.createdAt,project.updatedAt, project.user!!.email!!)
    }
    fun taskToDTO(task: Task) : TaskDTO{
        val taskDTO = TaskDTO(task.taskId!!, task.backlog!!.backlogId!!,task.projectTaskSequence!!,task.summary!!,
                task.acceptanceCriteria!!, task.status!!,task.priority!!,task.dueDate!!, task.createdAt ,task.updatedAt )
        return taskDTO
    }
    fun userToDTO(user: ApplicationUser) : UserDTO{
        val userDTO = UserDTO(user.userId, user.fullName,user.email)
        return userDTO
    }
}
