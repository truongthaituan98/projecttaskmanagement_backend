package com.example.demo.repositories

import com.example.demo.domain.entities.ApplicationUser
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface UserRepository : JpaRepository<ApplicationUser, Long> {
    fun findByemail(email: String): Optional<ApplicationUser>
    @Query("SELECT count(u.email)>0 FROM users u WHERE u.email=:email", nativeQuery = true)
    fun isEmailExist(@Param("email") email: String): Int

}
