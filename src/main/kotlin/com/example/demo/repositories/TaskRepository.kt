package com.example.demo.repositories

import com.example.demo.domain.entities.Task
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*
import kotlin.collections.ArrayList


@Repository
interface TaskRepository : JpaRepository<Task, Long>{
        @Query("SELECT * FROM tasks t1 where t1.task_id" +
            " IN( select t2.task_id from backlogs b, " +
            "tasks t2 where b.backlog_id = t2.backlog_id and b.project_identifier = :project_identifier)" +
            "group by t1.task_id",
            nativeQuery = true)
    fun showAllTask(@Param("project_identifier")projectIdentifier: String): ArrayList<Task>

    fun getTaskByprojectTaskSequence(projectTaskSequence: String): Optional<Task>

    @Query("SELECT * FROM tasks t1 where t1.task_id IN( select t2.task_id from backlogs b, tasks t2 " +
            " where b.backlog_id = t2.backlog_id and b.project_identifier = :project_identifier " +
            "and t2.project_task_sequence = :project_task_sequence)" +
            "group by t1.task_id", nativeQuery = true)
    fun showTaskByProjectTaskSequence(@Param("project_identifier")projectIdentifier: String,
                                      @Param("project_task_sequence")projectTaskSequence: String): Optional<Task>
    @Query("SELECT * FROM tasks t1 where t1.task_id IN( select t2.task_id from backlogs b, tasks t2 " +
            " where b.backlog_id = t2.backlog_id and b.project_identifier = :project_identifier " +
            "and t2.project_task_sequence = :project_task_sequence)" +
            "group by t1.task_id", nativeQuery = true)
    fun showTaskByProjectTaskSequenceTest(@Param("project_identifier")projectIdentifier: String,
                                      @Param("project_task_sequence")projectTaskSequence: String): Task
}
