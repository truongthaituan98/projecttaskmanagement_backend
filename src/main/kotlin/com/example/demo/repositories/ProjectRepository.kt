package com.example.demo.repositories

import com.example.demo.domain.entities.Project
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ProjectRepository : JpaRepository<Project, String>{
    fun findByprojectIdentifier(projectIdentifier: String): Optional<Project>

    @Query("select * from projects p where p.user_id in(\n" +
            "SELECT p.user_id FROM projects p, users u where p.user_id = u.id and u.email = :email)", nativeQuery = true)
    fun findAllProjectsByEmail(@Param("email") email: String): List<Project>

}
