package com.example.demo.repositories

import com.example.demo.domain.entities.TaskTotal
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import kotlin.collections.ArrayList


@Repository
interface TaskTotalRepository : JpaRepository<TaskTotal, Long> {
    @Query("select * from tasktotals tt where tt.backlog_id = :backlog_id",nativeQuery = true)
    fun getAllTaskTotal(@Param("backlog_id")backlogId: Long): ArrayList<TaskTotal>
}