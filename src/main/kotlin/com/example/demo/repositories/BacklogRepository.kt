package com.example.demo.repositories

import com.example.demo.domain.entities.Backlog
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BacklogRepository : JpaRepository<Backlog, Long>{
@Query("SELECT * FROM backlogs bl where bl.project_identifier = :project_identifier", nativeQuery = true)
fun getBacklogIdByProjectIdentifier(@Param("project_identifier") projectIdentifier: String): Optional<Backlog>

    @Query("SELECT * FROM backlogs bl where bl.backlog_id = :backlog_id", nativeQuery = true)
    fun getProjectIdentifierByBacklogId(@Param("backlog_id") backlogId: Long): Backlog

//    @Query("select * from backlogs where exists (select project_identifier from backlogs b, " +
//            "tasktotals tt where b.backlog_id = tt.backlog_id and b.backlog_id = :backlog_id)",nativeQuery = true)
    @Query("select * from backlogs where backlog_id IN( select tt.backlog_id from tasktotals tt where tt.backlog_id = :backlog_id)" +
            " group by backlog_id", nativeQuery = true)
    fun checkProjectExist(@Param("backlog_id")backlogId: Long): Optional<Backlog>

}
