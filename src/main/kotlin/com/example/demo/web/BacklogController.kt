package com.example.demo.web

import com.example.demo.domain.dto.BacklogDTO
import com.example.demo.domain.entities.Backlog
import com.example.demo.services.BacklogService
import org.springframework.web.bind.annotation.*

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.Authorization

@RestController
@RequestMapping("/api/backlogs")
class BacklogController(private val backlogService: BacklogService) {

    @GetMapping("")
    @ApiOperation(value = "Get user's favourite parking spaces", notes = "Retrieves user's favourite parking spaces.")
    fun getAllBacklogs(): List<BacklogDTO> =
            backlogService.findAllBacklog()

    @PostMapping("")
    fun createNewBacklog(@RequestBody backlog: Backlog): BacklogDTO = backlogService.insertBacklog(backlog)


//    @GetMapping("/{id}")
//    fun getBacklogById(@PathVariable(value = "id") backlogId: Long): ResponseEntity<BacklogDTO> {
//        return backlogService.getBacklogById(backlogId).map { backlog ->
//            ResponseEntity.ok(MapperUtil.backlogToDTO(backlog))
//        }.orElseThrow{ BacklogNotFoundException(backlogId) }
//    }

//    @GetMapping("/{project_identifier}")
//    fun getBacklogByProjectIdentifier(@PathVariable(value = "project_identifier") project_identifier: String):
//            ResponseEntity<TaskDTO> {
//        return backlogService.showAllTask(project_identifier).map { task ->
//            ResponseEntity.ok(MapperUtil.taskToDTO(task))
//        }.orElseThrow{ BacklogNotFoundException(project_identifier) }
//    }

//    @PutMapping("/update/{id}")
//    fun updateBacklogById(@PathVariable("id") id: Long, @RequestBody newBacklog: Backlog): ResponseEntity<BacklogDTO> {
//        val backlog = this.backlogService.getBacklogById(id)
//        backlog.(newBacklog.project!!.projectIdentifier)
//        this.backlogService.updateBacklog(backlog)
//        return ResponseEntity.status(HttpStatus.OK).body(MapperUtil.backlogToDTO(backlog))
//    }

//    @DeleteMapping("/delete/{id}")
//    fun deleteBacklogById(@PathVariable(value = "id") backlogId: Long): ResponseEntity<String>? {
//        return backlogService.deleteBacklog(backlogId)
//    }
}