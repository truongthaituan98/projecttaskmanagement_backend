package com.example.demo.web

import com.example.demo.domain.dto.AuthRequest
import com.example.demo.exception.CustomException
import com.example.demo.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
import com.example.demo.services.MapValidationErrorService
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.validation.BindingResult


//@CrossOrigin(origins = ["http://localhost:4200"])
@RestController
@RequestMapping("/api/users")
class AuthenticateController {
    @Autowired
    private val userService: UserService? = null
    @Autowired
    private val mapValidationErrorService: MapValidationErrorService? = null

    @PostMapping("/login")
    fun login(@Valid @RequestBody authRequest: AuthRequest,result: BindingResult ): ResponseEntity<*> {
        val errorMap = mapValidationErrorService!!.mapValidationService(result)
        if (errorMap != null) return errorMap
        try {

            return ResponseEntity(userService!!.login(authRequest.email, authRequest.password), HttpStatus.OK)
        } catch (ex: Exception) {
            throw CustomException("Username or Password is incorrect")
        }
    }
}
