package com.example.demo.web

import com.example.demo.domain.dto.UserDTO
import com.example.demo.domain.dto.UserRegisterDto
import com.example.demo.services.MapValidationErrorService
import com.example.demo.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping("/api/users")
class UserController(private val userService: UserService) {
    @Autowired
    private val mapValidationErrorService: MapValidationErrorService? = null
    @GetMapping("")
    fun getAllUser(): List<UserDTO> =
            userService.findAll()

    @PostMapping("/register")
    fun createNewUser(@Valid @RequestBody userRegisterDto: UserRegisterDto,result: BindingResult): Any {
        val errorMap = mapValidationErrorService!!.mapValidationService(result)
        if (errorMap != null) return errorMap
        return userService.registerUser(userRegisterDto)
    }
}