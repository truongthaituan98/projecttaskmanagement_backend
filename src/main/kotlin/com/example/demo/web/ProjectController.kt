package com.example.demo.web

import com.example.demo.domain.dto.CustomUserDetail
import com.example.demo.domain.dto.ProjectCreateDTO
import com.example.demo.domain.dto.ProjectDTO
import com.example.demo.domain.dto.ProjectUpdateDTO
import com.example.demo.domain.entities.Project
import com.example.demo.services.BacklogService
import com.example.demo.services.MapValidationErrorService
import com.example.demo.services.ProjectService
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
@RequestMapping("/api/projects")
class ProjectController {

    @Autowired
    lateinit var projectService: ProjectService

    @Autowired
    lateinit var backlogService: BacklogService

    @Autowired
    private val mapValidationErrorService: MapValidationErrorService? = null

    @GetMapping("")
    fun getAllProjects(): List<ProjectDTO> {
        val email = SecurityContextHolder.getContext().authentication.name
        println(email)
       return projectService.findAllProjectByEmail(email)
    }

    @PostMapping("")
    fun createNewProject(@Valid @RequestBody projectCreateDTO: ProjectCreateDTO, result: BindingResult): Any{
        val errorMap = mapValidationErrorService!!.mapValidationService(result)
        if (errorMap != null) return errorMap
        val user = (SecurityContextHolder.getContext().
                authentication.principal as CustomUserDetail).applicationUser
        return projectService.insertProject(user, projectCreateDTO)
    }

    @GetMapping("/{projectIdentifier}")
    fun getProjectByProjectIdentifier(@PathVariable(value = "projectIdentifier") projectIdentifier: String): Any {
            return  projectService.getProjectByProjectIdentifier(projectIdentifier)
    }

    @PutMapping("/{projectIdentifier}")
    fun updateProjectById(@PathVariable("projectIdentifier") projectIdentifier: String,
                          @Valid @RequestBody newProject: ProjectUpdateDTO, result: BindingResult): Any {
        val errorMap = mapValidationErrorService!!.mapValidationService(result)
        if (errorMap != null) return errorMap
        val user = (SecurityContextHolder.getContext().
                authentication.principal as CustomUserDetail).applicationUser
        return projectService.updateProject(user, projectIdentifier, newProject)
    }

    @DeleteMapping("/{projectIdentifier}")
    fun deleteProjectById(@PathVariable(value = "projectIdentifier") projectIdentifier: String): ResponseEntity<Any> {
            val project = this.projectService.getProjectByProjectIdentifier(projectIdentifier)
        return projectService.deleteProject(projectIdentifier)
    }
}