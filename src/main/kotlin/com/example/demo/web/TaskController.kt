package com.example.demo.web

import com.example.demo.common.MapperUtil
import com.example.demo.domain.dto.TaskCreateDTO
import com.example.demo.domain.dto.TaskDTO
import com.example.demo.domain.dto.TaskUpdateDTO
import com.example.demo.domain.entities.Task
import com.example.demo.services.BacklogService
import com.example.demo.services.MapValidationErrorService
import com.example.demo.services.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PatchMapping

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
@RequestMapping("/api")
class TaskController(private val taskService: TaskService, private val backlogService: BacklogService) {
    @Autowired
    private val mapValidationErrorService: MapValidationErrorService? = null
    @GetMapping("/backlog/{project_identifier}")
    fun getAllTask(@PathVariable(value = "project_identifier") project_identifier: String): List<TaskDTO> =
            taskService.findAllTaskByProjectIdentifier(project_identifier)

    @PostMapping("/backlog/{project_identifier}")
    fun createTask(@PathVariable(value = "project_identifier") project_identifier: String,
                   @Valid @RequestBody taskCreateDTO: TaskCreateDTO,
                    result: BindingResult): Any {
        val errorMap = mapValidationErrorService!!.mapValidationService(result)
        if (errorMap != null) return errorMap
        return taskService.createBacklogByProjectIdentifier(taskCreateDTO, project_identifier)
    }
    @GetMapping("/backlog/{project_identifier}/{project_task_sequence}")
    fun getTaskByProjectTask(@PathVariable(value = "project_identifier") project_identifier: String,
                             @PathVariable(value = "project_task_sequence") project_task_sequence: String): ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.
                taskToDTO(taskService.getTaskByProjectTaskSequence(project_identifier, project_task_sequence) as Task))
    }
    @DeleteMapping("/backlog/{project_identifier}/{project_task_sequence}")
    fun deleteTaskByProjectTask(@PathVariable(value = "project_identifier") project_identifier: String,
                             @PathVariable(value = "project_task_sequence") project_task_sequence: String): ResponseEntity<Void>{
         taskService.deleteByProjectTaskSequence(project_identifier, project_task_sequence)
          return ResponseEntity(HttpStatus.OK)
    }

    @PatchMapping("/backlog/{project_identifier}/{project_task_sequence}")
        fun updateProjectTask(@Valid @RequestBody projectTask: TaskUpdateDTO, result: BindingResult,
                          @PathVariable project_identifier: String, @PathVariable project_task_sequence: String): Any {

        val errorMap = mapValidationErrorService!!.mapValidationService(result)
        if (errorMap != null) return errorMap

        val updatedTask = taskService.
                updateByProjectTaskSequence(projectTask, project_identifier, project_task_sequence)

        return ResponseEntity(updatedTask, HttpStatus.OK)

    }

}