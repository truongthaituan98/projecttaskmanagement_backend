package com.example.demo.controller


import com.example.demo.domain.entities.Project
import com.example.demo.services.ProjectService
import com.example.demo.web.ProjectController
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.util.Arrays
import org.springframework.http.*
import org.mockito.Mockito.*
import org.mockito.Mockito.verifyNoMoreInteractions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.hamcrest.Matchers.hasSize
import org.hamcrest.core.Is.`is`
import java.time.LocalDate

@RunWith(SpringRunner::class)
@WebAppConfiguration
class ProjectControllerTest {
    private var mockMvc: MockMvc? = null
    @MockBean
    private val projectService: ProjectService? = null
    @InjectMocks
    lateinit var projectControllerTest: ProjectController

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        mockMvc = MockMvcBuilders
                .standaloneSetup(projectControllerTest)
                .build()
    }

//    @Test
//    @Throws(Exception::class)
//    fun test_get_all_success() {
//        val projects = Arrays.asList<Project>(
//                Project("KOTL1", "kotlin",
//        "fundamental kotlin",
//        LocalDate.now(), LocalDate.of(2020,6,12),
//        LocalDate.of(2020,6,12),LocalDate.of(2020,6,12))
//                , Project("KOTL2", "kotlin2",
//                "fundamental kotlin2",
//                LocalDate.of(2020,7,12), LocalDate.of(2020,7,14),
//                LocalDate.of(2020,7,14), LocalDate.of(2020,7,14))
//        )
//        `when`(projectService!!.findAll()).thenReturn(projects)
//        mockMvc!!.perform(get("/api/projects"))
//                .andExpect(status().isOk)
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(jsonPath("$", hasSize<Any>(2)))
//                .andExpect(jsonPath("$[0].projectIdentifier", `is`("KOTL1")))
//                .andExpect(jsonPath("$[0].projectName", `is`("kotlin")))
//                .andExpect(jsonPath("$[0].description", `is`("fundamental kotlin")))
//                .andExpect(jsonPath("$[1].projectIdentifier", `is`("KOTL2")))
//                .andExpect(jsonPath("$[1].projectName", `is`("kotlin2")))
//                .andExpect(jsonPath("$[1].description", `is`("fundamental kotlin2")))
//        verify(projectService, times(1)).findAll()
//        verifyNoMoreInteractions(projectService)
//    }

    @Test
    @Throws(Exception::class)
    fun test_get_project_by_id_success() {
        val project =  Project("KOTL1", "kotlin",
                "fundamental kotlin",
                LocalDate.now(), LocalDate.of(2020,6,12),
                LocalDate.of(2020,6,12),LocalDate.of(2020,6,12))

        `when`(projectService!!.getProjectByProjectIdentifier("KOTL1"))
                .thenReturn(java.util.Optional.ofNullable(project))

        mockMvc!!.perform(get("/api/projects/KOTL1", 1))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.projectIdentifier", `is`("KOTL1")))
                .andExpect(jsonPath("$.projectName", `is`("kotlin")))
                .andExpect(jsonPath("$.description", `is`("fundamental kotlin")))

        verify(projectService, times(1)).getProjectByProjectIdentifier("KOTL1")
        verifyNoMoreInteractions(projectService)
    }

}
