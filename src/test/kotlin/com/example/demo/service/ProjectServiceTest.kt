package com.example.demo.service


import com.example.demo.domain.dto.ProjectCreateDTO
import com.example.demo.domain.dto.ProjectDTO
import com.example.demo.domain.entities.Project
import com.example.demo.repositories.ProjectRepository
import com.example.demo.services.ProjectServiceImpl
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations

import org.springframework.boot.test.mock.mockito.MockBean

import org.springframework.test.context.junit4.SpringRunner

import org.junit.Assert.assertEquals
import org.mockito.Mockito.*
import java.time.LocalDate
import java.util.ArrayList
import java.util.Optional
@RunWith(SpringRunner::class)
class ProjectServiceTest {
    @InjectMocks
    lateinit var projectService: ProjectServiceImpl
    @MockBean
    private val projectRepository: ProjectRepository? = null
    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
    }

//    @Test
//    fun testGetAllProjects() {
//        val projects = ArrayList<Project>()
//        val project1 = Project("KOTL1", "kotlin",
//                "fundamental kotlin",
//                LocalDate.now(), LocalDate.of(2020,6,12),
//                LocalDate.of(2020,6,12),LocalDate.of(2020,6,12))
//        val project2 = Project("KOTL2", "kotlin2",
//                "fundamental kotlin2",
//                LocalDate.of(2020,7,12), LocalDate.of(2020,7,14),
//                LocalDate.of(2020,7,14),LocalDate.of(2020,7,14))
//        projects.add(project1)
//        projects.add(project2)
//        `when`(projectRepository!!.findAll()).thenReturn(projects)
//        val projectList = projectService.findAll()
//        assertEquals(2, projectList.size)
//        verify(projectRepository).findAll()
//    }

    @Test
    fun testGetProjectByProjectIdentifier() {
        `when`(projectRepository!!.findByprojectIdentifier("KOTL1"))
                .thenReturn(Optional.of<Project>(Project("KOTL1", "kotlin",
                        "fundamental kotlin",
                        LocalDate.now(), LocalDate.of(2020,6,12),
                        LocalDate.of(2020,6,12),LocalDate.of(2020,6,12))))
        val project = projectService.getProjectByProjectIdentifier("KOTL1") as ProjectDTO
        assertEquals("kotlin", project.projectName)
        assertEquals("KOTL1", project.projectIdentifier)
    }

//    @Test
//    fun testDeleteProjectByProjectIdentifier() {
//        val project = Project("KOTL9", "Learn about Kotlin",
//                "The first project", LocalDate.of(2020,6,12))
//        val optionalProject = Optional.of<Project>(project)
//        `when`(projectRepository!!.findByprojectIdentifier("KOTL9")).thenReturn(optionalProject)
//        projectService.deleteProject(project.projectIdentifier)
//    }

//    @Test
//    fun testCreateProject() {
//        val projectDTO = ProjectCreateDTO("KOTL9", "Learn about Kotlin",
//                "The first project","2020-06-12","2020-06-12")
//        val project = Project.createProjectDTOToProject(projectDTO)
//         projectRepository!!.save(project)
//    }
}
