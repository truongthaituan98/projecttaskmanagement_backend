package com.example.demo.service
import com.example.demo.domain.entities.Project
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations

import org.springframework.boot.test.mock.mockito.MockBean

import org.springframework.test.context.junit4.SpringRunner

import org.junit.Assert.assertEquals
import org.mockito.Mockito.*
import java.time.LocalDate
import com.example.demo.domain.entities.Backlog
import com.example.demo.domain.entities.Task
import com.example.demo.repositories.ProjectRepository
import com.example.demo.repositories.TaskRepository
import com.example.demo.services.*
import java.util.*


@RunWith(SpringRunner::class)
class TaskServiceTest {
    @InjectMocks
    lateinit var taskService: TaskServiceImpl
    @MockBean
    private val taskRepository: TaskRepository? = null

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
    }
    @Test
    fun showAllTaskByProjectIdentifier() {
        val tasks = ArrayList<Task>()
        val project1 = Project("KOTL1", "kotlin",
                "fundamental kotlin",
                LocalDate.now(), LocalDate.of(2020,6,12),
                LocalDate.of(2020,6,12),LocalDate.of(2020,6,12))
        val backlog = Backlog(1.toLong(),project1)
        val task1 = Task(1.toLong(),"Research react","abc","TO_DO","LOW",
                LocalDate.of(2020,7,14)
                ,LocalDate.of(2020,7,14),
                LocalDate.of(2020,7,14),backlog,"KOTL1-1")
        val task2 = Task(2.toLong(),"Research react","abc","TO_DO","LOW",
                LocalDate.of(2020,7,14)
                ,LocalDate.of(2020,7,14),
                LocalDate.of(2020,7,14),backlog,"KOTL1-2")
        tasks.add(task1)
        tasks.add(task2)
        `when`(taskRepository!!.showAllTask("KOTL1")).thenReturn(tasks)
        val taskList: ArrayList<Task> = taskService.findAllTaskByProjectIdentifierTest("KOTL1")
        assertEquals(2, taskList.size)
    }
    @Test
    fun getTaskByProjectTaskSequenceTest(){
        val project1 = Project("KOTL1", "kotlin",
                "fundamental kotlin",
                LocalDate.now(), LocalDate.of(2020,6,12),
                LocalDate.of(2020,6,12),LocalDate.of(2020,6,12))
        val backlog = Backlog(1.toLong(),project1)
        val task1 = Task(1.toLong(),"Research react","abc","TO_DO","LOW",
                LocalDate.of(2020,7,14)
                ,LocalDate.of(2020,7,14),
                LocalDate.of(2020,7,14),backlog,"KOTL1-1")
        `when`(taskRepository!!.showTaskByProjectTaskSequenceTest("KOTL1","KOTL1-1")).
                thenReturn(task1)
        assertEquals("abc", task1.acceptanceCriteria)
    }
    @Test
    fun testDeleteByProjectTaskSequence(){
        val project1 = Project("KOTL1", "kotlin",
                "fundamental kotlin",
                LocalDate.now(), LocalDate.of(2020,6,12),
                LocalDate.of(2020,6,12),LocalDate.of(2020,6,12))
        val backlog = Backlog(1.toLong(),project1)
        val task1 = Task(1.toLong(),"Research react","abc","TO_DO","LOW",
                LocalDate.of(2020,7,14)
                ,LocalDate.of(2020,7,14),
                LocalDate.of(2020,7,14),backlog,"KOTL1-1")
        `when`(taskRepository!!.showTaskByProjectTaskSequenceTest("KOTL1","KOTL1-1")).thenReturn(task1)
        taskService.deleteByProjectTaskSequenceTest(task1.backlog!!.project!!.projectIdentifier!!, task1.projectTaskSequence!!)
    }

}