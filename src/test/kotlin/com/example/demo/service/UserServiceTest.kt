package com.example.demo.service


import com.example.demo.domain.dto.UserRegisterDto
import com.example.demo.domain.entities.ApplicationUser
import com.example.demo.repositories.UserRepository
import com.example.demo.services.ProjectServiceImpl
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.*
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import org.junit.Assert.assertEquals
import java.util.*

@RunWith(SpringRunner::class)
class UserServiceTest {
    @InjectMocks
    lateinit var userService: ProjectServiceImpl
    @MockBean
    private val userRepository: UserRepository? = null

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
    }
    @Test
    fun testFindUserByEmail() {
        `when`(userRepository!!.findByemail("tuan1@gmail.com"))
                .thenReturn(Optional.of(ApplicationUser("tuan","tuan1@gmail.com","123456")))
        assertEquals("tuan", userRepository.findByemail("tuan1@gmail.com").get().fullName)
    }
    @Test
    fun testRegister() {
        val userRegister = UserRegisterDto("tuan", "tuan1@gmail.com",
                "123456","123456")
        val user = ApplicationUser(userRegister.email,userRegister.fullName,userRegister.password)

        userRepository!!.save(user)

    }
}